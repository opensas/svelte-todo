https://www.papercall.io/cfps/2326/submissions/new (2019/08/06)


https://www.papercall.io/talks/148594/children/148595

https://twitter.com/nerdearla?lang=es

---

https://www.papercall.io/nerdearla-2020 (2020-08-31)

https://www.papercall.io/cfps/3333/submissions/new

---

### Título
Desarrollo de aplicaciones web con Svelte: Ni librería ni framework... COMPILADOR!!!

### Elevator Pitch

Svelte es un enfoque radicalmente nuevo para crear interfaces de usuario. Mientras que los frameworks tradicionales hacen la mayor parte de su trabajo en el navegador, Svelte efectúa todo ese trabajo a un paso de compilación que ocurre cuando construimos nuestra aplicación. 


Svelte es un enfoque radicalmente nuevo para crear interfaces de usuario. Mientras que los frameworks tradicionales como React y Vue hacen la mayor parte de su trabajo en el navegador, Svelte cambia efectúa todo ese trabajo a un paso de compilación que ocurre cuando construimos nuestra aplicación. 

De esta manera, podremos escribir aplicaciones que no sólo cuentan con excelente rendimiento rendimiento, sino que también proveen una experiencia mucho más amigable para el desarrollador.

Svelte promete hacer más felices a los usuarios y desarrolladores, juntos desarrollaremos una aplicación web desde cero para ver qué hay de cierto en ello.

### Descripción

En los últimos años el desarrollo de interfaces de usuario se ha vuelto sumamente engorroso y complejo. Atrás parecen haber quedado los días en que con conocimientos de HTML, CSS y JavaScript podríamos aventurarnos en el mundo del desarrollo web, y hoy esa tarea parece reservada para desarrolladores "profesionales".

SvelteJS aporta un enfoque radicalmente nuevo, que no sólo permite construir aplicaciones sumamente livianas y de gran rendimiento, aporta una experiencia mucho más amigable para el desarrollador.

En esta charla discutiremos las principales características de esta herramienta y daremos los primeros pasos para desarrollar una aplicación web completa.

Para ello, seguiremos la serie de [tutoriales acerca de Svelte](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials) que publicamos recientemente en Mozilla Developer Network (disponibles en línea en https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials).

Para ello veremos cómo iniciar una aplicación Svelte, cómo crear nuestros propios componentes, qué opciones tenemos para que se comuniquen entre sí, cómo gestionar el estado de nuestra UI, cómo desplegar nuestras aplicaciones y, finalmente, qué se siente desarrollar aplicaciones web con Svelte.

### Notas

El único requerimiento técnico es poder contar con un proyector. Conexión wifi es altamente deseable para que el auditorio pueda ir probando los ejemplos de código.
Para aprovechar la charla es recomendable que los asistentes tengan un conocimiento mínimo de HTML, CSS y JavaScript. Quienes van a estar en mejores condiciones de apreciar y evaluar las ventajas y desventajas del enfoque adoptado por Svelte serán aquéllos con alguna experiencia en React, Vue, Angular o algún otro framework/librería para el desarrollo de aplicaciones web.

Ya tuve la oportunidad de hacer esta charla con desarrolladores con gran experiencia con React y Angular en varias oportunidades (nardoz), y felizmente el resultado fue muy positivo, la crítica de Svelte a estas soluciones y su novedoso enfoque despertó gran interés (y picantes debates) entre los presentes, por lo que confío en que será un aporte valioso a esta nueva edición de nerdearla.

EL tutorial de MDN que utilizaré como guía (y para que los asistentes puedan continuar aprendiendo por su cuenta): https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials


La aplicación completa con su código fuente: https://svelte.dev/repl/378dd79e0dfe4486a8f10823f3813190?version=3.23.2

Y esta es una presentación que utilicé anteriormente (ia idea es actualizarla para mostrar el material que elaboramos para MDN): [https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/](https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/)

### Tus datos

* **Bio**:
Mi nombre es Sebastián Scarano. Soy Ingeniero en Sistemas de la UTN y hace años que me dedico a desarrollar aplicaciones. Soy defensor del software libre y demás libertades. Me gusta aprender de los demás y compartir mis experiencias. Participo de Nardoz, HackHackers y demás comunidades dateras y nerd. Recientemente comencé a escribir artículos y documentación técnica para Mozilla Developer Network.

* **Foto**:
https://gitpitch.com/pitchme/cdn/gitlab/opensas/svelte-todo/master/F9A4D9CDC0107424CBF0D27462E4BE0D7C761B3D251F33E8AD3AA76446888AA30D2F05E879CA82345507E78A9A5C586D0D3041187F42BE34299AC353C6AA79CEA475A606AC6CA7D1BC6619EF19CF55B54907F2197069F9DF721F56C86B0A17DC/assets/img/opensas_avatar.jpg

* **Usuario de Slack**:
opensas (Sebastián Scarano)

* **Usuario de Twitter**: @opensas

* **Otros usuarios**: Si no tenés Twitter dejanos acá otros links donde te gustaría que te mencionemos en los tweets y otras publicaciones.

* **Pronombre**:
opensas

### Recursos

* **Slides de la presentación**: https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/

* **Repositorio con el código fuente del tutorial**: https://github.com/opensas/mdn-svelte-tutorial

* **Tutorial en línea**: https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials

### Extra

* **¿Tenés la charla preparada?**: Sí

> Avisanos si necesitás algo especial para poder dar la charla así lo podemos planificar con tiempo.

WIFI!!!! y proyector
