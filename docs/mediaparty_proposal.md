Qué nos puede enseñar Mozilla respecto al desarrollo de aplicaciones web: un ejemplo con SvelteJS

La actividad consistirá en un workshop en el cual daremos una introducción a SvelteJs, una herramienta que permite desarrollar sitios web interactivos y ágiles, y luego desarrollaremos e implementaremos una aplicación web.

Durante la primera parte mostraremos el uso y las posibilidades que nos brinda esta herramienta desarrollando iterativamente una aplicación para llevar registro de la lista de cosas que tenemos para hacer (un Todo application) y en la segunda parte daremos los primeros pasos para desarrollar una aplicación web.

Para ello, utilizaremos una serie de tutoriales que escribimos recientemente para Mozilla Developer Network y que se encuentra disponible aquí: https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials.

La idea es que los participantes se familiaricen con Svelte, y con el material de consulta provisto por Mozilla, a fin de que puedan completar el resto de los ejercicios del tutorial trabajando por su cuenta luego de finalizado el workshop.

SvelteJS aporta un nuevo y novedoso enfoque al desarrollo de interfaces de usuario. En vez de proveer un framework que corre en el explorador del usuario, SvelteJs compila nuestra aplicación a código JavaScript altamente optimizado. De esta manera, no sólo logramos una aplicación más liviana, sino también un proceso de desarrollo mucho más simple y ágil. Con tener tan sólo conocimientos básicos de HTML, CSS y JavaScrips ya podemos hacer aplicaciones completas.

SvelteJs fue creado por Rich Harris, periodista de investigación de The Guardian y del New York Times, y la herramienta creada por el es sumamente ágil, pragmática y simple. Precisamente por la facilidad que presenta para empezar a desarrollar, y lo livianas que resultan las aplicaciones desarrolladas con SvelteJS, es una herramienta ideal para realizar visualizaciones interactivas y embeber aplicaciones en sitios web.

Mozilla Developer Network (MDN) es uno de los recursos más populares para desarrolladores en la Web. Diseñado por desarrolladores para desarrolladores, MDN ayuda a respaldar la misión de Mozilla de promover la apertura y la innovación en la Web. En la actualidad, MDN brinda servicios a más de 15 millones de desarrolladores web mensualmente, poniendo a disposición la mejor documentación, tutoriales y herramientas para desarrolladores.

En los últimos meses estuvimos trabajando junto a MDN desarrollando una serie de tutoriales interactivos para aprender a desarrollar aplicaciones web con Svelte. El tutorial arranca de un nivel introductorio, llegando a cubrir aspectos avanzados de la herramienta. Provee además el código fuente de la aplicación y herramientas para ir desarrollando la aplicación paso a paso siguiendo el tutorial.

---

What Mozilla can teach us about web application development: an example with Svelte

The activity will consist of a workshop in which we will give an introduction to Svelte, a tool that allows developing interactive and agile websites, and then we will develop and implement a web application.

During the first part we will show the use and possibilities that this tool offers us by iteratively developing an application to keep track of the list of things we have to do (a Todo application) and in the second part we will take the first steps to develop a web application .

To do this, we will use a series of tutorials that we recently wrote for the Mozilla Developer Network and are available here: https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials.

The idea is to help participants become familiar with Svelte and the reference material provided by Mozilla, so that they can complete the rest of the tutorial exercises working on their own after the workshop is finished.

Svelte brings a fresh new approach to user interface development. Instead of providing a framework that runs in the user's browser, Svelte compiles our application to highly optimized JavaScript code. In this way, we not only achieve a lighter application, but also a much simpler and more agile development process. With only having basic knowledge of HTML, CSS and JavaScrips we can already make complete applications.

Svelte was created by Rich Harris, an investigative journalist for The Guardian and the New York Times, and the tool created by him is extremely agile, pragmatic and simple. Precisely because of the ease it presents to start developing, and how light the applications developed with Svelte are, it is an ideal tool for making interactive visualizations and embedding applications on websites.

The Mozilla Developer Network (MDN) is one of the most popular developer resources on the Web. Designed by developers for developers, MDN helps support Mozilla's mission to promote openness and innovation on the Web. Today, MDN serves more than 15 million web developers on a monthly basis, making the best documentation, tutorials, and developer tools available.

In the last few months we have been working with MDN developing a series of interactive tutorials to learn how to develop web applications with Svelte. The tutorial starts from an introductory level ut then covers advanced aspects of the tool. It also provides the source code of the application and tools to develop the application step by step following the tutorial.