*Svelte: ni framework ni librería... ¡COMPILADOR!*

Este *martes* doy una charla en Nardoz acerca de *SvelteJS*: el framework para desarrollo de aplicaciones web que no es un framework.

Se trata de una alternativa a React, Vue, Angular, etc. que aporta un nuevo y radical enfoque al desarrollo de aplicaciones. Las soluciones tradicionales facilitan el desarrollo de aplicaciones a un costo, el código (del framework o la librería) corre en tu explorador y ocupa recursos. En cambio Svelte construye a partir de tu código, y en tiempo de compilación, una aplicación en "plain JavaScript" optimizada para generar la UI de tu aplicación.

La cita es *este martes, 16 de julio, a partir de las 19:00 en AreaTres* (El Salvador 5218, Palermo https://goo.gl/maps/tdP9y3PMr6HqfkZWA)

Para asistir *deben registrarse aquí*: https://www.eventbrite.com/e/nardoz-julio-tickets-65115222383

Y si quieren empezar a jugar con *SvelteJs* pueden seguir el tutorial interactivo: https://svelte.dev/tutorial/basics

please RT: https://twitter.com/NardozBA/status/1125469970408906753

Saludos

Sas

--

La actividad consistirá en un workshop de 3 horas en el cual daremos una introducción a SvelteJs, una herramienta que permite desarrollar sitios web interactivos y ágiles, y luego desarrollaremos e implementaremos una aplicación web.

Durante la primera parte mostraremos el uso y las posibilidades que nos brinda esta herramienta desarrollando iterativamente una aplicación para llevar registro de la lista de cosas que tenemos para hacer (un Todo application) y en la segunda parte iremos resolviendo ejercicios interactivos hasta desarrollar y deplegar una aplicación básica que nos permita interactuar con algún servicio web.

SvelteJS aporta un nuevo y novedoso enfoque al desarrollo de interfaces de usuario. En vez de proveer un framework que corre en el explorador del usuario, SvelteJs compila nuestra aplicación a código JavaScript altamente optimizado. De esta manera, no sólo logramos una aplicación más liviana, sino también un proceso de desarrollo mucho más simple y ágil. Con tener tan sólo conocimientos básicos de HTML, CSS y JavaScrips ya podemos hacer aplicaciones completas.

SvelteJs fue creado por Rich Harris, periodista de investigación de The Guardian y del New York Times, y la herramienta creada por el es sumanmente ágil, pragmática y simple. Precisamente por la facilidad que presenta para empezar a desarrollar, y lo livianas que resultan las aplicaciones desarrolladas con SvelteJS, es una herramienta ideal para realizar visualizaciones interactivas y embeber applicaciones en sitios web.

--

y esto es lo que di ayer:
repo: https://gitlab.com/opensas/svelte-todo
slides: https://gitpitch.com/opensas/svelte-todo?grs=gitlab#/
app: https://todo.opensas.now.sh/
