oh hello my name is Elgin McLaren and

I'd like to talk about creative coding

and opening up open source specifically

I'd like to talk about p5.js what it is

and what we as developers can learn from

there intentionally inclusive open

source community so p5.js is a

JavaScript library that makes it super

easy to build interactive art in the

browser using the HTML canvas element

it's really fun to use and it's super

easy to get started with so I would like

to get people excited about how they can

use p5.js in their own project by

showing some really cool examples of the

work that's been done with p5.js next

I'd like to get into a bit more of the

technical details what is it about ppas

it's gonna make your life as a developer

easier then I'd like to talk about my

experience working with them as part of

people Summer of Code and give kind of

an insider insight into what I've

learned while I was working about the

organization one thing I think is really

great about p5 is that although it's

used by developers it was actually built

with artists and non coders in mind and

as a result inclusivity and

accessibility is like just core to all

of the work that they do and I think it

would be really great to be able to

finish my talk by going over some of the

specific practices and activities that

the p5 community engages in to help

support encouraging the users so I think

this talk is gonna be really fun because

it's an opportunity to create art in the

browser and do something kind of

different than just a normal web

development project it's super easy to

use it's really cool and I really like

using it in my own work and I think it's

gonna also give us a really great

opportunity to start talking about how

we can make our own opens

he's more accessible to people who are

new to coding so a little bit about me

I'm a master student at the school of

interactive Arts and Technology in

Vancouver Canada and I previously worked

as a web developer I've been a lot

involved a lot in coding education

projects in the past and I'm super

excited about this toffee and I hope you

will consider my application thank you

