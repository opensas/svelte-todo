# Cuál es la mejor manera para que dos componentes que no están vinculados jerárquicamente se comuniquen entre sí?

[ ] Props

[ ] Eventos

[X] Stores

[ ] Two-way binding


# Cómo implementa Svelte la reactividad?

[ ] Mediante getters y setters

[ ] Usando una implementación propia de RxJS

[X] Instrumentando nuestro código para actualizar las variables según sus dependencias

[ ] Utilizando virtual DOM


# Qué ejecutamos en el explorador cuando corremos una aplicación generada con Svelte?

[ ] Nuestro código JavaScript

[X] Un conjunto mínimo de funciones JavaScript y nuestro código instrumentado

[ ] El runtime de svelte

[ ] Nuestro código compilado a un JavaScript altamente optimizado


# Cómo indicamos una sentencia reactiva en Svelte?

[ ] #:

[X] $:

[ ] ?:

[ ] !:


# Cómo instrumenta Svelte la sentencia "count += 1"?

[ ] this.setState({ count: count + 1 })

[X] $$invalidate('count', count += 1)

[ ] count = count + 1

[ ] $$state.count = $$state.count + 1


# Svelte... (marcá todas las que correspondan)

[X] Agrega la primitiva `$:` a JavaScript para transformarlo en un lenguaje reactivo

[X] Extiende CSS para que los estilos de distintos componentes no se superpongan

[ ] Implementa un dom virtual en tiempo de compilación

[X] Extiende HTML agregando directivas para manejar bucles, condicionales y responder a eventos


# Para implementar reactividad Svelte: (marcá todas las que correspondan)

[X] Instrumenta las asignaciones a variables y propiedades

[ ] Genera setters compilados que se disparan cuando cambia el estado de nuestra aplicación

[X] Reinterpreta el comando `$:` para indicar sentencias reactivas

[X] Genera código JavaScript que se ejecuta en orden topológico para actualizar las respectivas dependencias


# En definitiva... qué es Svelte?

[ ] Un framework para el desarrollo de aplicaciones web

[X] Una extensión a JavaScript, HTML y CSS implementado a través de un compilador

[ ] Una librería de JavaScript para crear interfaces de usuario

[ ] Un compilador que genera código JavaScript altamente optimizado
