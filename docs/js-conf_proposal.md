js conf

https://devchat.tv/conferences/javascript-remote-2020/?__s=9do9zxswe4mpa1ewtq7a

https://devchattv.typeform.com/to/XXLjow

Svelte is shaking up the front-end development world with it's radical new approach to building user interfaces. Instead of delivering a shiny new framework that does the bulk of their work in the browser while the app is running, like traditional frameworks do, Svelte shifts that work into a compile step that happens only when you build your app, producing highly optimized plain vanilla JavaScript code.

The outcome of this approach is not only leaner and faster apps, but also a developer experience that is much more approachable for common people with no web-development degrees under his belt.

In this presentation we will have a look at Svelte proposal, how it compares to traditional frameworks and libraries, and we will also build together a fully working web applicaction.
