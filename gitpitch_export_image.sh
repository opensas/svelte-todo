# source: https://docs.docker.com/engine/reference/commandline/save/#save-an-image-to-a-targz-file-using-gzip
docker save gitpitch/free:latest | gzip > gitpitch-free_latest.tar.gz

# import: https://docs.docker.com/engine/reference/commandline/import/#import-from-a-local-file
# docker import /path/to/exampleimage.tgz