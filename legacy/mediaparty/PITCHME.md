<!-- ---?image=assets/dr_js/img01.jpg -->

@snap[north-west]
#### Qué nos puede enseñar
@snapend

@snap[north-east snap-100 fragment]
## Mozilla
@snapend

@snap[middle snap-100 fragment]
### respecto al desarrollo de aplicaciones web
@snapend

@snap[south-west snap-100 fragment]
#### Un ejemplo con
@snapend

@snap[south-east snap-100 fragment]
# Svelte
@snapend

---

@snap[north span-50]
### Acerca de mí...
@snapend

@snap[west span-33]
![IMAGE](assets/img/opensas_avatar.jpg)
@snapend

@snap[east span-67]
@size[0.7em](- Ingeniero de la UTN <br>)
@size[0.7em](- Haciendo sistemas en el Ministerio de Trabajo <br>)
@size[0.7em](- Defensor del software libre y otras libertades <br>)
@size[0.7em](- Participo en la comunidad datera y nerd <br>)
@size[0.7em](- y ahora también en MDN web docs <br>)
@snapend

@snap[south]
twitter: [@opensas](https://twitter.com/opensas)
@snapend

---

@snap[north span-100]
### Acerca de <br />MDN Web Docs
@snapend

@snap[west span-20]
![IMAGE](assets/img/mdn/mdn-web-docs.png)
@snapend

@snap[east span-80]
@size[0.5em](- el "manual de Internet para desarrolladores" <br>)
@size[0.5em](- sitio oficial de Mozilla para la documentación de desarrollo y estándares web <br>)
@size[0.5em](- wiki mantenida por la comunidad de desarrolladores de todo el mundo<br>)
@size[0.5em](- aloja miles documentos técnicos de primer nivel<br>)
@size[0.5em](- actualmente es consultada por más de 15 millones de desarrolladores al mes! <br>)
@size[0.5em](- recientemente agregó una sección para <a href="https://developer.mozilla.org/en-US/docs/Learn#Where_to_start">aprender desarrollo web</a> <br>)
@size[0.5em](- ...y también otra acerca de <a href="https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials">frameworks de desarrollo frontend</a> <br>)
@snapend

@snap[south text-06]
twitter: [@MozDevNet](https://twitter.com/MozDevNet) - [@chrisdavidmills](https://twitter.com/chrisdavidmills)
@snapend

---

@snap[north-west text-05 span-90]
### Por qué es tan complicado hacer aplicaciones web?
@snapend

@snap[north-east text-04 span-10]
#### [source](https://codesandbox.io/s/web-apps-static-vs-dynamic-jolbt?file=/dynamic.html)
@snapend

@snap[west span-50 fragment]
[<img src="assets/img/static-dynamic/static-html.png">](https://static-dynamic.vercel.app/static.html)
@snapend

@snap[east span-50 fragment]
[<img src="assets/img/static-dynamic/dynamic-html.png">](https://static-dynamic.vercel.app/dynamic.html)
@snapend

@snap[south-east text-04 span-100 fragment]
[Static example](https://codesandbox.io/s/web-apps-static-vs-dynamic-jolbt?file=/static.html) |
[Dynamic example](https://codesandbox.io/s/web-apps-static-vs-dynamic-jolbt?file=/dynamic.html)
@snapend

---

@snap[north text-06 span-100]
### 30 años de desarrollo web en 2 slides...
@snapend

@snap[text-08 fragment]

<ul>

<li>1990 - 1995: surgen HTML, CSS and JavaScript, los pilares de la programación web.</li>

<li>1996 - 2004: Las páginas son mayormente estáticas, cada interacción supone cargar de vuelta la página.</li>

<li>2005: jQuery se instala como la librería dominante en desarrollo web (api más amigable, salva las inconsistencias entre los browsers)</li>

<li>2006: Google presenta Google Maps, la primera aplicación AJAX, usando una olvidada tecnología de 1999 (XMLHttpRequest)</li>

</ul>

@snapend

---

@snap[north text-06 span-100]
### 30 años de desarrollo web en 2 slides...
@snapend

@snap[text-08 fragment]

<ul>

<li>2010 Backbone: la primera librería orientada a crear Single Page Applications (SPA)</li>

<li>2010 AngularJS: provee una arquitectura completa para el desarrollo front-end. Trajo data binding bi-directional y la posibilidad de crear componentes reutilizables</li>

<li>2013 React: basado en la idea de componentes y el Virtual DOM, tomando elementos de la programación funcional (state driven apps, enfoque declarativo, sin mutaciones)</li>

<li>2014 Vue: en algunos aspectos es un punto intermedio entre Angular y React, con una curva de aprendizaje más liviana.</li>

</ul>

@snapend

---

@snap[north span-100]

### La filosofía de Svelte...
@fa[quote-left quote-graphql text-white](Hacer más con menos)

@snapend

@snap[midpoint span-60]
![SVELTE](assets/img/svelte.png)
@snapend

@snap[south-west span-30 text-08 fragment]
@box[rounded text-white box-graphql](Menos código...#para el usuario<br>&nbsp;)
@snapend

@snap[south span-30 text-08 fragment]
@box[rounded text-white box-graphql](Menos código...#para el desarrollador)
@snapend

@snap[south-east span-30 text-08 fragment]
@box[rounded text-white box-graphql](Menos código...#para ejecutar<br>&nbsp;)
@snapend

+++

@snap[north-west text-06 span-100]
### Infaltable meme gratuito...
@snapend

@snap[midpoint text-08 span-100]
![SIZE](assets/img/svelte_react_meme.jpg)
@snapend

---
@snap[north text-06 span-100]
## Cuál es el secreto de Svelte?
@snapend

@snap[text-08]

<ul>

<li class="fragment">Svelte "compila" nuestro código produciendo JavaScript "limpio" (vanilla-js) altamente optimizado en tiempo de compilación</li>

<li class="fragment">Genera aplicaciones más livianas con mejor rendimiento</li>

<li class="fragment">Provee una experiencias mucho más amigable para el desarrollador</li>

<li class="fragment">Adhiere más estrechamente al modelo de desarrollo web clásico de HTML, CSS y JS, agregando algunas extensiones a HTML y JavaScript</li>

</ul>

@snapend

@snap[south-east text-04 span-100]
source: [Svelte MDN Tutorial](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_getting_started#Svelte_A_new_approach_to_building_rich_user_interfaces)
@snapend

---

@snap[north-west text-06 span-100]
### Menos código para el usuario... (size matters)
@snapend

@snap[midpoint text-08 span-100 fragment img-svelte-compared-size]
![SIZE](assets/img/svelte_compared_size.png)
@snapend

@snap[south-east text-04 span-100]
source: [A RealWorld Comparison of Front-End Frameworks](https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/)
@snapend

---

@snap[north-west text-06 span-100]
### Menos código para el dev... (happiness matters)
@snapend

@snap[midpoint text-08 span-100 fragment img-svelte-compared-loc]
![SIZE](assets/img/svelte_compared_loc.png)
@snapend

@snap[south-east text-04 span-100]
source: [A RealWorld Comparison of Front-End Frameworks](https://www.freecodecamp.org/news/a-realworld-comparison-of-front-end-frameworks-with-benchmarks-2019-update-4be0d3c78075/)
@snapend

---

@snap[north-west text-05 span-80]
### Menos código para el dev... (happiness matters)
@snapend

@snap[north-east text-04 span-20]
#### [working example](https://codesandbox.io/s/svelte-less-code-yjowc?file=/App.svelte)
@snapend

@snap[west fragment]
![REACT](assets/img/loc/react_loc.png)
@snapend

@snap[midpoint fragment]
![VUE](assets/img/loc/vue_loc.png)
@snapend

@snap[east fragment]
![SVELTE](assets/img/loc/svelte_loc.png)
@snapend

@snap[south-east text-04 span-100 fragment]
[React example](https://codesandbox.io/s/lesscode-sveltevsreeact-ujt7h) |
[Vue example](https://codesandbox.io/s/lesscode-sveltevsvue-sok3i) |
[Svelte example](https://codesandbox.io/s/svelte-less-code-yjowc?file=/App.svelte)
@snapend

---

@snap[north-west text-10 span-100]
### Menos código para el dev
@snapend

@snap[midpoint text-07 span-100]
<blockquote>
<p>Death to boilerplate... How?</p>

<p>
Because Svelte is a compiler, we're not bound to the peculiarities of JavaScript: we can design a component authoring experience, rather than having to fit it around the semantics of the language. Paradoxically, this results in more idiomatic code — for example using variables naturally rather than via proxies or hooks — while delivering significantly more performant apps.
</p>
</blockquote>
@snapend

@snap[south-east text-04 span-100]
Source: [Death to boilerplate](https://svelte.dev/blog/write-less-code#Death_to_boilerplate)
@snapend
---

@snap[north-west text-10 span-100]
### Menos código para ejecutar
##### [Virtual DOM is pure overhead](https://rethinking-reactivity.surge.sh/#slide=9)
@snapend

@snap[midpoint text-05 span-100]
<blockquote>
<p>Why do frameworks use the virtual DOM then?</p>

<p>It's important to understand that virtual DOM isn't a feature. It's a means to an end, the end being declarative, state-driven UI development. Virtual DOM is valuable because it allows you to build apps without thinking about state transitions, with performance that is generally good enough.</p>

<p>But it turns out that we can achieve a similar programming model without using virtual DOM — and that's where Svelte comes in.</p>
</blockquote>
@snapend

@snap[south-east text-04 span-100]
Source: [Virtual DOM is pure overhead](https://svelte.dev/blog/virtual-dom-is-pure-overhead#Why_do_frameworks_use_the_virtual_DOM_then)
@snapend

---

## Reactividad en serio

- [Rich Harris - Rethinking reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao) | [presentation](https://rethinking-reactivity.surge.sh)

- Mike Boston's [observable](https://rethinking-reactivity.surge.sh/#slide=19)

- [Destiny operator](https://rethinking-reactivity.surge.sh/#slide=20)

---

#### Cómo le decimos a la computadora<br> que un valor cambió

##### <br>Con una api...

```javascript
const { count } = this.state;
this.setState({
  count: count + 1
});
```

---

#### Cómo le decimos a la computadora<br> que un valor cambió

##### <br>O mejor sin ninguna api..

```javascript
count += 1;
```

##### <br>...y que el compilador se encargue de "instrumentarlo"

```javascript
count += 1; $$invalidate('count', count);
```

---

@snap[north text-06 span-100]
## Qué tiene Mozilla para decirnos<br />acerca de Svelte?
@snapend

@snap[text-08]

<ul>

<li class="fragment">Un <a href="https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials">tutorial en línea</a> en el que desarrollamos una aplicación completa desde cero.</li>

<li class="fragment">Ocho capítulos cubriendo temas iniciales y avanzados.</li>

<li class="fragment">Un <a href="https://github.com/opensas/mdn-svelte-tutorial">repositorio en github</a> para descargar el código fuente de cada paso</li>

<li class="fragment">Una <a href="https://svelte.dev/repl/378dd79e0dfe4486a8f10823f3813190?version=3.23.2">plataforma interactiva</a> (Svelte REPL) para seguir el tutorial en línea, sin instalarse nada en nuestro equipo</li>

</ul>

@snapend

---

@snap[north text-06 span-100]
## Cómo seguir el tutorial?
@snapend

@snap[text-08]

<ul>

<li class="fragment">Descargando el repositorio completo: <br /><code>git clone https://github.com/opensas/mdn-svelte-tutorial.git</code></li>

<li class="fragment">Descargando cada paso por separado: <br /><code>npx degit opensas/mdn-svelte-tutorial/02-starting-our-todo-app</code></li>

<li class="fragment">Trabajando en línea con el <a href="https://github.com/opensas/mdn-svelte-">REPL</a> de cada capítulo. (Por ejemplo, aquí tienen el <a href="https://svelte.dev/repl/378dd79e0dfe4486a8f10823f3813190?version=3.23.2">REPL</a> de la aplicación terminada)</li>

</ul>


@snap[south-east text-04 span-100]
Source: [Following this tutorial](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_getting_started#Following_this_tutorial)
@snapend

---

@snap[north text-10 span-100]
## A codear!
@snapend

@snap[midpoint span-100]
![SIZE](assets/img/enough_talk.jpg)
@snapend

@snap[south-east text-04 span-20]
#### [Ver app online](https://todo.opensas.now.sh/)
@snapend

---

#### Svelte: Cómo seguir?

@snap[text-08]

<ul>

<li class="fragment">Completar <a href="https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials">el tutorial de MDN</a></a></li>

<li class="fragment">Ver la presentación <a href="https://www.youtube.com/watch?v=AdNJ3fydeao">Rethinking Reactivity</a></li>

<li class="fragment">Seguir el <a href="https://svelte.dev/tutorial/basics">tutorial interactivo</a></li>

<li class="fragment">Ver <a href="https://sapper.svelte.dev/">Sapper</a>: The next small thing in web development</li>

<li class="fragment">Ver <a href="https://svelte-native.technology">Svelte Native</a>: The Svelte Mobile Development Experience</li>

---?image=assets/images/gitpitch-audience.jpg

</ul>

@snapend

---?image=assets/images/gitpitch-audience.jpg

### ¿Preguntas?

<br>

@fa[twitter gp-contact]&nbsp;[@opensas](https://twitter.com/opensas)

@fa[github gp-contact]&nbsp;[repo at github](https://github.com/opensas/mdn-svelte-tutorial)

@fa[firefox gp-contact]&nbsp;[tutorial on MDN web docs](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks#Svelte_tutorials
)

@fa[chalkboard-teacher gp-contact]&nbsp;[these slides](https://gitpitch.com/opensas/svelte-todo?grs=gitlab&p=mediaparty#/)

@fa[list-alt gp-contact]&nbsp;[application on line](https://svelte.dev/repl/378dd79e0dfe4486a8f10823f3813190?version=3.23.2)

---

#### Agradecimientos

### A todos ustedes!

<br>

@fa[twitter gp-contact]&nbsp;[@Rich_Harris](https://twitter.com/Rich_Harris) - Rich Harris

@fa[twitter gp-contact]&nbsp;[@Sveltejs](https://twitter.com/Sveltejs) - La comunidad Svelte

@fa[twitter gp-contact]&nbsp;[@MozDevNet](https://twitter.com/@MozDevNet) - MDN Web Docs y [@Mozilla](https://twitter.com/mozilla)

@fa[twitter gp-contact]&nbsp;[@chrisdavidmills](https://twitter.com/@chrisdavidmills) - Chris Mills de MDN
