# Svelte-todo demo cheatsheet

## Preparativos

1. Abrir links

## Mostrar presentación

local: [http://localhost:9000/gitpitch/desktop?p=mediaparty#/](http://localhost:9000/gitpitch/desktop?p=mediaparty#/)

remoto: [https://gitpitch.com/opensas/svelte-todo?grs=gitlab&p=mediaparty#/](https://gitpitch.com/opensas/svelte-todo?grs=gitlab&p=mediaparty#/)

## Demo

### 1. Create todo svelte app

```bash
$ cd /home/sas/devel/apps/svelte/mediaparty-2020
$ npx degit sveltejs/template moz-todo-svelte
$ cd moz-todo-svelte
$ npm install
$ npm run dev
```

1. Mostrar la aplicación

- package.json
- rollup.config.js
- src/main.js
- src/App.svelte

navegar a: http://localhost:5000/

Mostrar scoped css

mostrar como se conecta todo
- public/index.html
- public/bundle.js
- public/bundle.css

remplazar App.svelte por

#SNIP

```svelte
<script>
  export let name = 'world' ;

  function toggleName() {
    if (name === 'world') {
      name = 'svelte'
    } else {
      name = 'world'
    }
  }
</script>

<main>
  <h1>Hello {name}!</h1>
  <button on:click={toggleName}>Toggle name</button>
  <p>Visit the <a href="https://svelte.dev/tutorial">Svelte tutorial</a> to learn how to build Svelte apps.</p>
</main>
```

Summary 

### 2. Bajar el repositorio de la aplicación

Mostrar cómo bajar el código de la aplicación

```shell
cd /home/sas/devel/apps/svelte/mediaparty-2020
npx degit opensas/mdn-svelte-tutorial mdn-svelte-tutorial


```
cd /home/sas/devel/apps/svelte/mediaparty-2020/mdn-svelte-tutorial`

cd 02-starting-our-todo-app/

npm install

npm run dev
```

### Crear Todos.svelte

crear archivo src/components/Todos.svelte

#SNIP
src/components/Todos.svelte
```svelte
<h1>Svelte To-Do list</h1>
```

#SNIP
src/App.svelte
```svelte
<script>
  import Todos from './components/Todos.svelte'
</script>

<Todos />
```


### Static markup


#SNIP
src/components/Todos.svelte
```html
<!-- Todos.svelte -->
<div class="todoapp stack-large">

  <!-- NewTodo -->
  <form>
    <h2 class="label-wrapper">
      <label for="todo-0" class="label__lg">
        What needs to be done?
      </label>
    </h2>
    <input type="text" id="todo-0" autocomplete="off"
      class="input input__lg" />
    <button type="submit" disabled="" class="btn btn__primary btn__lg">
      Add
    </button>
  </form>

  <!-- Filter -->
  <div class="filters btn-group stack-exception">
    <button class="btn toggle-btn" aria-pressed="true">
      <span class="visually-hidden">Show</span>
      <span>All</span>
      <span class="visually-hidden">tasks</span>
    </button>
    <button class="btn toggle-btn" aria-pressed="false">
      <span class="visually-hidden">Show</span>
      <span>Active</span>
      <span class="visually-hidden">tasks</span>
    </button>
    <button class="btn toggle-btn" aria-pressed="false">
      <span class="visually-hidden">Show</span>
      <span>Completed</span>
      <span class="visually-hidden">tasks</span>
    </button>
  </div>

  <!-- TodosStatus -->
  <h2 id="list-heading">2 out of 3 items completed</h2>

  <!-- Todos -->
  <ul role="list" class="todo-list stack-large" aria-labelledby="list-heading">

    <!-- todo-1 (editing mode) -->
    <li class="todo">
      <div class="stack-small">
        <form class="stack-small">
          <div class="form-group">
            <label for="todo-1" class="todo-label">
              New name for 'Create a Svelte starter app'
            </label>
            <input type="text" id="todo-1" autocomplete="off" class="todo-text" />
          </div>
          <div class="btn-group">
            <button class="btn todo-cancel" type="button">
              Cancel
              <span class="visually-hidden">renaming Create a Svelte starter app</span>
            </button>
            <button class="btn btn__primary todo-edit" type="submit">
              Save
              <span class="visually-hidden">new name for Create a Svelte starter app</span>
            </button>
          </div>
        </form>
      </div>
    </li>

    <!-- todo-2 -->
    <li class="todo">
      <div class="stack-small">
        <div class="c-cb">
          <input type="checkbox" id="todo-2" checked/>
          <label for="todo-2" class="todo-label">
            Create your first component
          </label>
        </div>
        <div class="btn-group">
          <button type="button" class="btn">
            Edit
            <span class="visually-hidden">Create your first component</span>
          </button>
          <button type="button" class="btn btn__danger">
            Delete
            <span class="visually-hidden">Create your first component</span>
          </button>
        </div>
      </div>
    </li>
    
    <!-- todo-3 -->
    <li class="todo">
      <div class="stack-small">
        <div class="c-cb">
          <input type="checkbox" id="todo-3" />
          <label for="todo-3" class="todo-label">
            Complete the rest of the tutorial
          </label>
        </div>
        <div class="btn-group">
          <button type="button" class="btn">
            Edit
            <span class="visually-hidden">Complete the rest of the tutorial</span>
          </button>
          <button type="button" class="btn btn__danger">
            Delete
            <span class="visually-hidden">Complete the rest of the tutorial</span>
          </button>
        </div>
      </div>
    </li>
  </ul>

  <hr />

  <!-- MoreActions -->
  <div class="btn-group">
    <button type="button" class="btn btn__primary">Check all</button>
    <button type="button" class="btn btn__primary">Remove completed</button>
  </div>

</div>
```


#SNIP
public/global.css

```css
/* RESETS */
*,
*::before,
*::after {
  box-sizing: border-box;
}
*:focus {
  outline: 3px dashed #228bec;
  outline-offset: 0;
}
html {
  font: 62.5% / 1.15 sans-serif;
}
h1,
h2 {
  margin-bottom: 0;
}
ul {
  list-style: none;
  padding: 0;
}
button {
  border: none;
  margin: 0;
  padding: 0;
  width: auto;
  overflow: visible;
  background: transparent;
  color: inherit;
  font: inherit;
  line-height: normal;
  -webkit-font-smoothing: inherit;
  -moz-osx-font-smoothing: inherit;
  -webkit-appearance: none;
}
button::-moz-focus-inner {
  border: 0;
}
button,
input,
optgroup,
select,
textarea {
  font-family: inherit;
  font-size: 100%;
  line-height: 1.15;
  margin: 0;
}
button,
input {
  overflow: visible;
}
input[type="text"] {
  border-radius: 0;
}
body {
  width: 100%;
  max-width: 68rem;
  margin: 0 auto;
  font: 1.6rem/1.25 Arial, sans-serif;
  background-color: #f5f5f5;
  color: #4d4d4d;
}
@media screen and (min-width: 620px) {
  body {
    font-size: 1.9rem;
    line-height: 1.31579;
  }
}
/*END RESETS*/

/* GLOBAL STYLES */
.form-group > input[type="text"] {
  display: inline-block;
  margin-top: 0.4rem;
}
.btn {
  padding: 0.8rem 1rem 0.7rem;
  border: 0.2rem solid #4d4d4d;
  cursor: pointer;
  text-transform: capitalize;
}
.btn.toggle-btn {
  border-width: 1px;
  border-color: #d3d3d3;
}
.btn.toggle-btn[aria-pressed="true"] {
  text-decoration: underline;
  border-color: #4d4d4d;
}
.btn__danger {
  color: #fff;
  background-color: #ca3c3c;
  border-color: #bd2130;
}
.btn__filter {
  border-color: lightgrey;
}
.btn__primary {
  color: #fff;
  background-color: #000;
}
.btn__primary:disabled {
  color: darkgrey;
  background-color:#565656;
}
.btn-group {
  display: flex;
  justify-content: space-between;
}
.btn-group > * {
  flex: 1 1 49%;
}
.btn-group > * + * {
  margin-left: 0.8rem;
}
.label-wrapper {
  margin: 0;
  flex: 0 0 100%;
  text-align: center;
}
.visually-hidden {
  position: absolute !important;
  height: 1px;
  width: 1px;
  overflow: hidden;
  clip: rect(1px 1px 1px 1px);
  clip: rect(1px, 1px, 1px, 1px);
  white-space: nowrap;
}
[class*="stack"] > * {
  margin-top: 0;
  margin-bottom: 0;
}
.stack-small > * + * {
  margin-top: 1.25rem;
}
.stack-large > * + * {
  margin-top: 2.5rem;
}
@media screen and (min-width: 550px) {
  .stack-small > * + * {
    margin-top: 1.4rem;
  }
  .stack-large > * + * {
    margin-top: 2.8rem;
  }
}
.stack-exception {
  margin-top: 1.2rem;
}
/* END GLOBAL STYLES */

.todoapp {
  background: #fff;
  margin: 2rem 0 4rem 0;
  padding: 1rem;
  position: relative;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 2.5rem 5rem 0 rgba(0, 0, 0, 0.1);
}
@media screen and (min-width: 550px) {
  .todoapp {
    padding: 4rem;
  }
}
.todoapp > * {
  max-width: 50rem;
  margin-left: auto;
  margin-right: auto;
}
.todoapp > form {
  max-width: 100%;
}
.todoapp > h1 {
  display: block;
  max-width: 100%;
  text-align: center;
  margin: 0;
  margin-bottom: 1rem;
}
.label__lg {
  line-height: 1.01567;
  font-weight: 300;
  padding: 0.8rem;
  margin-bottom: 1rem;
  text-align: center;
}
.input__lg {
  padding: 2rem;
  border: 2px solid #000;
}
.input__lg:focus {
  border-color: #4d4d4d;
  box-shadow: inset 0 0 0 2px;
}
[class*="__lg"] {
  display: inline-block;
  width: 100%;
  font-size: 1.9rem;
}
[class*="__lg"]:not(:last-child) {
  margin-bottom: 1rem;
}
@media screen and (min-width: 620px) {
  [class*="__lg"] {
    font-size: 2.4rem;
  }
}
.filters {
  width: 100%;
  margin: unset auto;
}
/* Todo item styles */
.todo {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
}
.todo > * {
  flex: 0 0 100%;
}
.todo-text {
  width: 100%;
  min-height: 4.4rem;
  padding: 0.4rem 0.8rem;
  border: 2px solid #565656;
}
.todo-text:focus {
  box-shadow: inset 0 0 0 2px;
}
/* CHECKBOX STYLES */
.c-cb {
  box-sizing: border-box;
  font-family: Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  font-weight: 400;
  font-size: 1.6rem;
  line-height: 1.25;
  display: block;
  position: relative;
  min-height: 44px;
  padding-left: 40px;
  clear: left;
}
.c-cb > label::before,
.c-cb > input[type="checkbox"] {
  box-sizing: border-box;
  top: -2px;
  left: -2px;
  width: 44px;
  height: 44px;
}
.c-cb > input[type="checkbox"] {
  -webkit-font-smoothing: antialiased;
  cursor: pointer;
  position: absolute;
  z-index: 1;
  margin: 0;
  opacity: 0;
}
.c-cb > label {
  font-size: inherit;
  font-family: inherit;
  line-height: inherit;
  display: inline-block;
  margin-bottom: 0;
  padding: 8px 15px 5px;
  cursor: pointer;
  touch-action: manipulation;
}
.c-cb > label::before {
  content: "";
  position: absolute;
  border: 2px solid currentColor;
  background: transparent;
}
.c-cb > input[type="checkbox"]:focus + label::before {
  border-width: 4px;
  outline: 3px dashed #228bec;
}
.c-cb > label::after {
  box-sizing: content-box;
  content: "";
  position: absolute;
  top: 11px;
  left: 9px;
  width: 18px;
  height: 7px;
  transform: rotate(-45deg);
  border: solid;
  border-width: 0 0 5px 5px;
  border-top-color: transparent;
  opacity: 0;
  background: transparent;
}
.c-cb > input[type="checkbox"]:checked + label::after {
  opacity: 1;
}
```

### Agregamos todos


#SNIP
components/Todos.svelte
```svelte
<script>
  let todos = [
    { id: 1, name: 'Create a Svelte starter app', completed: true },
    { id: 2, name: 'Create your first component', completed: true },
    { id: 3, name: 'Complete the rest of the tutorial', completed: false }
  ]

  let totalTodos = todos.length
  let completedTodos = todos.filter(todo => todo.completed).length
</script>
```

#SNIP
components/Todos.svelte
```svelte
  <!-- TodosStatus -->
  <h2 id="list-heading">{completedTodos} out of {totalTodos} items completed</h2>
```

#SNIP
```svelte
components/Todos.svelte
<ul>
{#each todos as todo, index (todo.id)}
  <li>
    <input type="checkbox" checked={todo.completed}/> {index}. {todo.name} (id: {todo.id})
  </li>
{:else}
  Nothing to do here!
{/each}
</ul>
```

#SNIP
components/Todos.svelte
```svelte
<!-- Todos -->
<ul role="list" class="todo-list stack-large" aria-labelledby="list-heading">
  {#each todos as todo (todo.id)}
    <li class="todo">
      <div class="stack-small">
        <div class="c-cb">
          <input type="checkbox" id="todo-{todo.id}" checked={todo.completed}/>
          <label for="todo-{todo.id}" class="todo-label">
            {todo.name}
          </label>
        </div>
        <div class="btn-group">
          <button type="button" class="btn">
            Edit <span class="visually-hidden">{todo.name}</span>
          </button>
          <button type="button" class="btn btn__danger">
            Delete <span class="visually-hidden">{todo.name}</span>
          </button>
        </div>
      </div>
    </li>
  {:else}
    <li>Nothing to do here!</li>
  {/each}
</ul>
```

Mover el array de todos a App.svelte, pasarlo como una propiedad

#SNIP
App.svelte
```svelte
<script>
  import Todos from './components/Todos.svelte'

  let todos = [
    { id: 1, name: 'Create a Svelte starter app', completed: true },
    { id: 2, name: 'Create your first component', completed: true },
    { id: 3, name: 'Complete the rest of the tutorial', completed: false }
  ]

</script>

<Todos {todos}/>
```

toggling todo


#SNIP
Todos.svelte
```svelte
          <input type="checkbox" id="todo-{todo.id}" 
            on:click={() => todo.completed = !todo.completed}
            checked={todo.completed}
          />
```

#SNIP
Todos.svelte
```svelte
  function removeTodo(todo) {
    todos = todos.filter(t => t.id !== todo.id)
  }
```

#SNIP
Todos.svelte
```svelte
<button type="button" class="btn btn__danger"
  on:click={() => removeTodo(todo)}
>
```

#SNIP
Todos.svelte
```svelte
$: totalTodos = todos.length
$: completedTodos = todos.filter(todo => todo.completed).length
```

### Adding new todos


#SNIP
Todos.svelte
```javascript
  let newTodoName = ''
```

#SNIP
Todos.svelte
```svelte
    <input bind:value={newTodoName} 
      type="text" id="todo-0" autocomplete="off" class="input input__lg" 
    />
```

#SNIP
Todos.svelte
```svelte
  function addTodo() {
    todos.push({ id: 999, name: newTodoName, completed: false })
    newTodoName = ''
  }
```

#SNIP
Todos.svelte
```svelte
<form on:submit|preventDefault={addTodo}>
```

ver por qué no anda (todos.push)

#SNIP
Todos.svelte
```svelte
  function addTodo() {
    todos = [...todos, { id: 999, name: newTodoName, completed: false }]
    newTodoName = ''
  }
```

calcular el nuevo id

#SNIP
Todos.svelte
```javascript
let newTodoId
  $: {
    if (totalTodos === 0) {
      newTodoId = 1
    } else {
      newTodoId = Math.max(...todos.map(t => t.id)) + 1
    }
  }
```

#SNIP
Todos.svelte
```svelte
  $: newTodoId = (totalTodos === 0) ? 1 : Math.max(...todos.map(t => t.id)) + 1
```

filtar los todos por estado

#SNIP
Todos.svelte
```javascript
let filter = 'all'
  const filterTodos = (filter, todos) =>
    filter === 'active' ? todos.filter(t => !t.completed) :
    filter === 'completed' ? todos.filter(t => t.completed) :
    todos
```

#SNIP
Todos.svelte
```svelte
  <!-- Filter -->
  <div class="filters btn-group stack-exception">
    <button class="btn toggle-btn" class:btn__primary={filter === 'all'} aria-pressed={filter === 'all'} on:click={()=> filter = 'all'} >
      <span class="visually-hidden">Show</span>
      <span>All</span>
      <span class="visually-hidden">tasks</span>
    </button>
    <button class="btn toggle-btn" class:btn__primary={filter === 'active'} aria-pressed={filter === 'active'} on:click={()=> filter = 'active'} >
      <span class="visually-hidden">Show</span>
      <span>Active</span>
      <span class="visually-hidden">tasks</span>
    </button>
    <button class="btn toggle-btn" class:btn__primary={filter === 'completed'} aria-pressed={filter === 'completed'} on:click={()=> filter = 'completed'} >
      <span class="visually-hidden">Show</span>
      <span>Completed</span>
      <span class="visually-hidden">tasks</span>
    </button>
  </div>
```

Summary

04. Organizando nuestra aplicación en componentes

### FilterButton


#SNIP
Filter.svelte
```svelte
<script>
  export let filter = 'all'
</script>

<div class="filters btn-group stack-exception">
  <button class="btn toggle-btn" class:btn__primary={filter === 'all'} aria-pressed={filter === 'all'} on:click={()=> filter = 'all'} >
    <span class="visually-hidden">Show</span>
    <span>All</span>
    <span class="visually-hidden">tasks</span>
  </button>
  <button class="btn toggle-btn" class:btn__primary={filter === 'active'} aria-pressed={filter === 'active'} on:click={()=> filter = 'active'} >
    <span class="visually-hidden">Show</span>
    <span>Active</span>
    <span class="visually-hidden">tasks</span>
  </button>
  <button class="btn toggle-btn" class:btn__primary={filter === 'completed'} aria-pressed={filter === 'completed'} on:click={()=> filter = 'completed'} >
    <span class="visually-hidden">Show</span>
    <span>Completed</span>
    <span class="visually-hidden">tasks</span>
  </button>
</div>
```

#SNIP
Todos.svelte
```javascript
import FilterButton from './FilterButton.svelte'
```

#SNIP
Todos.svelte
```svelte
  <!-- Filter -->
  <FilterButton {filter} />
```

### Todo.Svelte


#SNIP
Todos.svelte
```svelte
<script>
  export let todo
</script>

<div class="stack-small">
  <div class="c-cb">
    <input type="checkbox" id="todo-{todo.id}"
      on:click={() => todo.completed = !todo.completed}
      checked={todo.completed}
    />
    <label for="todo-{todo.id}" class="todo-label">{todo.name}</label>
  </div>
  <div class="btn-group">
    <button type="button" class="btn">
      Edit <span class="visually-hidden">{todo.name}</span>
    </button>
    <button type="button" class="btn btn__danger" on:click={() => alert('not implemented')}>
      Delete <span class="visually-hidden">{todo.name}</span>
    </button>
  </div>
</div>
```

#SNIP
Todos.svelte
```javascript
import Todo from './Todo.svelte'
```

#SNIP
Todos.svelte
```svelte
  <!-- Todos -->
  <ul role="list" class="todo-list stack-large" aria-labelledby="list-heading">
    {#each filterTodos(filter, todos) as todo (todo.id)}
      <li class="todo">
        <Todo {todo} />
      </li>
    {:else}
      <li>Nothing to do here!</li>
    {/each}
  </ul>
```

### Props-down, events-up pattern - remove todo

#SNIP
Todos.svelte
```javascript
  import { createEventDispatcher } from 'svelte'
  const dispatch = createEventDispatcher()
```

#SNIP
Todos.svelte
```svelte
    <button type="button" class="btn btn__danger" on:click={() => dispatch('remove', todo)}>
      Delete <span class="visually-hidden">{todo.name}</span>
    </button>
```

#SNIP
Todos.svelte
```svelte
        <Todo {todo} on:remove={event => removeTodo(event.detail)}/>
```

### Update todo

#SNIP
Todos.svelte
```svelte
<script>
  import { createEventDispatcher } from 'svelte'
  const dispatch = createEventDispatcher()
  
  export let todo

  let editing = false                     // track editing mode
  let name = todo.name                    // hold the name of the todo being edited

  function update(updatedTodo) {
    todo = { ...todo, ...updatedTodo }    // applies modifications to todo
    dispatch('update', todo)              // emit update event
  }

  function onCancel() {
    name = todo.name                      // restores name to its initial value and
    editing = false                       // and exit editing mode
  }

  function onSave() {
    update({ name: name })                // updates todo name
    editing = false                       // and exit editing mode
  }

  function onRemove() {
    dispatch('remove', todo)              // emit remove event
  }

  function onEdit() {
    editing = true                        // enter editing mode
  }

  function onToggle() {
    update({ completed: !todo.completed}) // updates todo status
  }
</script>

<div class="stack-small">
{#if editing}
  <!-- markup for editing todo: label, input text, Cancel and Save Button -->
    <form on:submit|preventDefault={onSave} class="stack-small" on:keydown={e => e.key === 'Escape' && onCancel()}>
    <div class="form-group">
      <label for="todo-{todo.id}" class="todo-label">New name for '{todo.name}'</label>
      <input bind:value={name} type="text" id="todo-{todo.id}" autoComplete="off" class="todo-text" />
    </div>
    <div class="btn-group">
      <button class="btn todo-cancel" on:click={onCancel} type="button">
        Cancel<span class="visually-hidden">renaming {todo.name}</span>
        </button>
      <button class="btn btn__primary todo-edit" type="submit" disabled={!name}>
        Save<span class="visually-hidden">new name for {todo.name}</span>
      </button>
    </div>
  </form>
{:else}
  <!-- markup for displaying todo: checkbox, label, Edit and Delete Button -->
    <div class="c-cb">
    <input type="checkbox" id="todo-{todo.id}"
      on:click={onToggle} checked={todo.completed}
    >
    <label for="todo-{todo.id}" class="todo-label">{todo.name}</label>
  </div>
  <div class="btn-group">
    <button type="button" class="btn" on:click={onEdit}>
      Edit<span class="visually-hidden"> {todo.name}</span>
    </button>
    <button type="button" class="btn btn__danger" on:click={onRemove}>
      Delete<span class="visually-hidden"> {todo.name}</span>
    </button>
  </div>
{/if}
</div>
```

#SNIP
Todos.svelte
```svelte
  const updateTodo = (todo) => {
    const i = todos.findIndex(t => t.id === todo.id)
    todos[i] = todo
  }
```

#SNIP
Todos.svelte
```svelte
      <li class="todo">
        <Todo {todo} 
          on:remove={e => removeTodo(e.detail)}
          on:update={e => updateTodo(e.detail)}
          />
      </li>
```

### Stores - 06_stores

#SNIP
src/stores.js
```javascript
import { writable } from 'svelte/store'

export const alert = writable('Welcome to the To-Do list app!')
```

#SNIP
components/Alert.svelte
```svelte
<script>
  import { alert } from '../stores.js'
  import { onDestroy } from 'svelte'

  let alertContent = ''

  const unsubscribe = alert.subscribe(value => alertContent = value)

  onDestroy(unsubscribe)
</script>

{#if alertContent}
<div on:click={() => alertContent = ''}>
  <p>{ alertContent }</p>
</div>
{/if}

<style>
div {
  position: fixed;
  cursor: pointer;
  margin-right: 1.5rem;
  margin-left: 1.5rem;
  margin-top: 1rem;
  right: 0;
  display: flex;
  align-items: center;
  border-radius: 0.2rem;
  background-color: #565656;
  color: #fff;
  font-size: 0.875rem;
  font-weight: 700;
  padding: 0.5rem 1.4rem;
  font-size: 1.5rem;
  z-index: 100;
  opacity: 95%;
}
div p {
  color: #fff;
}
div svg {
  height: 1.6rem;
  fill: currentColor;
  width: 1.4rem;
  margin-right: 0.5rem;
}
</style>
```

#SNIP
App.svelte
```svelte
<!-- App.svelte -->
<script>
  import Alert from './components/Alert.svelte'
  import Todos from './components/Todos.svelte'

  let todos = [
    { id: 1, name: 'Create a Svelte starter app', completed: true },
    { id: 2, name: 'Create your first component', completed: true },
    { id: 3, name: 'Complete the rest of the tutorial', completed: false }
  ]
</script>

<Alert />
<Todos {todos} />
```

#SNIP
App.svelte
```svelte
<script>
  import { alert } from '../stores.js'
</script>

{#if $alert}
<div on:click={() => $alert = ''}>
  <p>{ $alert }</p>
</div>
{/if}
```

#SNIP
Todos.svelte
```javascript
<!-- components/Todos.svelte -->
<script>
  import FilterButton from './FilterButton.svelte'
  import Todo from './Todo.svelte'
  import MoreActions from './MoreActions.svelte'
  import NewTodo from './NewTodo.svelte'  
  import TodosStatus from './TodosStatus.svelte'
  
  import { alert } from '../stores'

  export let todos = []

  let todosStatus                   // reference to TodosStatus instance

  $: newTodoId = todos.length ? Math.max(...todos.map(t => t.id)) + 1 : 1

  function removeTodo(todo) {
    todos = todos.filter(t => t.id !== todo.id)
    todosStatus.focus()             // give focus to status heading
    $alert = `Todo '${todo.name}' has been deleted`
  } 

  function addTodo(name) {
    todos = [...todos, { id: newTodoId, name, completed: false }]
    $alert = `Todo '${name}' has been added`
  }

  function updateTodo(todo) {
    const i = todos.findIndex(t => t.id === todo.id)
    if (todos[i].name !== todo.name)            $alert = `todo '${todos[i].name}' has been renamed to '${todo.name}'`
    if (todos[i].completed !== todo.completed)  $alert = `todo '${todos[i].name}' marked as ${todo.completed ? 'completed' : 'active'}`
    todos[i] = { ...todos[i], ...todo }
  }

  let filter = 'all'
  const filterTodos = (filter, todos) => 
    filter === 'active' ? todos.filter(t => !t.completed) :
    filter === 'completed' ? todos.filter(t => t.completed) : 
    todos

  $: {
    if (filter === 'all')               $alert = 'Browsing all todos'
    else if (filter === 'active')       $alert = 'Browsing active todos'
    else if (filter === 'completed')    $alert = 'Browsing completed todos'
  }

  const checkAllTodos = (completed) => {
    todos = todos.map(t => ({...t, completed}))
    $alert = `${completed ? 'Checked' : 'Unchecked'} ${todos.length} todos`
  }

  const removeCompletedTodos = () => {
    $alert = `Removed ${todos.filter(t => t.completed).length} todos`
    todos = todos.filter(t => !t.completed)
  }
</script>
```

#SNIP
App.svelte
```svelte
<script>
  import { onDestroy } from 'svelte'
  import { alert } from '../stores.js'

  export let ms = 3000
  let visible
  let timeout

  const onMessageChange = (message, ms) => {
    clearTimeout(timeout)
    if (!message) {               // hide Alert if message is empty
      visible = false
    } else {
      visible = true                                              // show alert
      if (ms > 0) timeout = setTimeout(() => visible = false, ms) // and hide it after ms milliseconds
    }
  }
  $: onMessageChange($alert, ms)      // whenever the alert store or the ms props changes run onMessageChange
 
  onDestroy(()=> clearTimeout(timeout))           // make sure we clean-up the timeout

</script>

{#if visible}
<div on:click={() => visible = false}>
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/></svg>
  <p>{ $alert }</p>
</div>
{/if}
```
