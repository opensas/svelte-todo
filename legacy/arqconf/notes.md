# Frontend Architecture

https://martinfowler.com/architecture/

Martin Fowler

> Like many in the software world, I’ve long been wary of the term “architecture” as it often suggests a separation from programming and an unhealthy dose of pomposity. But I resolve my concern by emphasizing that good architecture is something that supports its own evolution, and is deeply intertwined with programming.

A second common style of definition for architecture is that it it's “the design decisions that need to be made early in a project”, but Ralph complained about this too, saying that it was more like the decisions you wish you could get right early in a project.

 “Architecture is about the important stuff. Whatever that is”.

recognizing what elements are likely to result in serious problems should they not be controlled.

https://www.youtube.com/watch?v=DngAZyWMGR0

## Definiciones

- Organización fundamental de los componentes de un sistema, sus relaciones entre sí y con el medio ambiente, y los principios que guían su diseño y evolución.

- Decisiones estructurales costosas (o percibidas como costosas) de cambiar una vez implementadas

- Estructura macro de un sistema, el diseño de la estructura de un sistema al más alto nivel de abstracción

- Las cuestiones importantes, cualesquiera sean (The important stuff—whatever that is)

- Los aspectos más importantes de

the most important aspects of the internal design of a software system

---

https://martinfowler.com/ieeeSoftware/whoNeedsArchitect.pdf

(Patrones de arquitecturas de aplicaciones empresariales)

a word we use when we want to talk about design but want to puff it up to make it sound important.

the shared understanding that the expert developers have of the system design.

the design decisions that need to be made early in a project

the decisions you wish you could get right early in a project.

Architecture is about the important stuff. Whatever that is.

