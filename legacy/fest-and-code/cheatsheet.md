# Svelte-todo demo cheatsheet

## Preparativos

1. Abrir links

2. bajar repos y dependencias

```shell
cd /home/sas/devel/apps/svelte/svelte-todo/fest-and-code
rm -fr mdn-svelte-tutorial
npx degit opensas/mdn-svelte-tutorial mdn-svelte-tutorial
cd mdn-svelte-tutorial
./update-all.sh
```

## Mostrar presentación

local: [http://localhost:9000/gitpitch/desktop?p=fest-and-code#](http://localhost:9000/gitpitch/desktop?p=fest-and-code#)

remoto: [https://gitpitch.com/opensas/svelte-todo?grs=gitlab&p=fest-and-code#](https://gitpitch.com/opensas/svelte-todo?grs=gitlab&p=fest-and-code#)

## Demo

### 1. Create todo svelte app

```bash
cd /home/sas/devel/apps/svelte/svelte-todo/fest-and-code
npx degit sveltejs/template moz-todo-svelte
cd moz-todo-svelte
npm install
npm run dev
```

1. Mostrar la aplicación

- package.json
- rollup.config.js
- src/main.js
- src/App.svelte

navegar a: http://localhost:5000/

Mostrar scoped css

mostrar como se conecta todo
- public/index.html
- public/bundle.js
- public/bundle.css

remplazar App.svelte por **01-App.svelte (App.svelte)**

explicar reactividad - mostar bundle.js

Summary

### 2. Bajar el repositorio de la aplicación

Mostrar cómo bajar el código de la aplicación

(NO! seguir trabajando con la app que creamos! moz-todo-svelte)

```
cd /home/sas/devel/apps/svelte/svelte-todo/fest-and-code
cd 02-starting-our-todo-app/
npm install
npm run dev
```

### Crear Todos.svelte

crear archivo src/components/Todos.svelte **02-todo-list-title (Todos.svelte)**

importarlos desde App.svelte **03-import-Todos.svelte (App.svelte)**

### Static markup

remplazar Todos.svelte con **04-Todos.svelte-complete-markup (Todos.svelte)**

remplazar public/global.css from **05-global.css-style (public/global.css)**

### Agregamos todos

script section de Todos.svelte con **06-create-todos-array (Todos.svelte)**

TodosStatus con **07-TodosStatus-with-variables (Todos.svelte)**

```svelte
  <!-- TodosStatus -->
  <h2 id="list-heading">{completedTodos} out of {totalTodos} items completed</h2>
```

Todos.svelte creamos un each de prueba **08-each-test (Todos.svelte)**
    <ul>
    {#each todos as todo, index (todo.id)}
      <li>
        <input type="checkbox" checked={todo.completed}/> {index}. {todo.name} (id: {todo.id})
      </li>
    {:else}
      Nothing to do here!
    {/each}
    </ul>
    }

Todos.svelte <!-- Todos --> con **09-each-loop-with-markup (Todos.svelte)**

Mover el array de todos a App.svelte, pasarlo como una propiedad

Remplazamos App.svelte con **10-pass-todos-array-from-App.svelte (App.svelte)**

Mostrar el warning de la consola!

```
<App> was created with unknown prop 'name' bundle.js:1001:78
<Todos> was created with unknown prop 'todos'
```

toggling todo: remplazamos el input del checkbox con **11-toggling-todos (Todos.svelte)**

#SNIP 11
Todos.svelte
```svelte
          <input type="checkbox" id="todo-{todo.id}"
            on:click={() => todo.completed = !todo.completed}
            checked={todo.completed}
          />
```

remove todo: creamos funcion removeTodo: **12-removeTodo-function (Todos.svelte)**

```javascript
  function removeTodo(todo) {
    todos = todos.filter(t => t.id !== todo.id)
  }
```

llamamos a removeTodo al apretar el boton: replace button con **13-call-removeTodo-from-remove-button (Todos.svelte)**

Todos.svelte
```svelte
<button type="button" class="btn btn__danger"
  on:click={() => removeTodo(todo)}
>
```

Declaramos totalTodos y completedTodos de manera reactiva: **14-reactive-variables-totalTodos-completedTodos (Todos.svelte)**

#SNIP 14
Todos.svelte
```svelte
$: totalTodos = todos.length
$: completedTodos = todos.filter(todo => todo.completed).length
```

### Adding new todos

**15-declare-newTodoName-variable (Todos.svelte)**
Todos.svelte
```javascript
  let newTodoName = ''
```

Mostramos un bind manual

```svelte
    <input type="text" id="todo-0" autocomplete="off"
      value={newTodoName} on:keyup={e => newTodoName = e.target.value }
      class="input input__lg" />
```

usamos bind:value: **16-bind-value-newTodoName-input (Todos.svelte)**
```svelte
    <input bind:value={newTodoName} type="text" id="todo-0" autocomplete="off" class="input input__lg"
    />
```

creamos funcion addTodo **17-addTodo-function (Todos.svelte)**
```svelte
  function addTodo() {
    todos.push({ id: 999, name: newTodoName, completed: false })
    newTodoName = ''
  }
```

llamamos a addTodo desde el submit del form: **18-form-submit-call-addTodo (Todos.svelte)**
```svelte
<form on:submit|preventDefault={addTodo}>
```

ver por qué no anda (todos.push)

#SNIP 19
Todos.svelte
```svelte
  function addTodo() {
    todos = [...todos, { id: 999, name: newTodoName, completed: false }]
    newTodoName = ''
  }
```

calcular el nuevo id: **20-calculate-new-id-long-version (Todos.svelte)**

#SNIP 21
Todos.svelte
```svelte
  $: newTodoId = (totalTodos === 0) ? 1 : Math.max(...todos.map(t => t.id)) + 1
```

filtrar los todos por estado: **22-fiterTodos-function (Todos.svelte)**

actualizar el markup <-- Filter --> **23-filterTodos-markup (Todos.svelte)**

actualizar el bucle!!

```svelte
{#each filterTodos(filter, todos) as todo (todo.id)}
```

Summary

04. Organizando nuestra aplicación en componentes

### FilterButton

creamos components/FilterButton.svelte: **40-FilterButton.svelte component (FilterButton.svelte)**

lo importamos en Todos.svelte: **41-import-FilterButton.svelte (Todos.svelte)**
```javascript
import FilterButton from './FilterButton.svelte'
```

lo usamos: **42-use-FilterButton (Todos.svelte markup)**
```svelte
  <!-- Filter -->
  <FilterButton {filter} />
```

Explicamos por que no funciona, agregamos el bind
```svelte
  <!-- Filter -->
  <FilterButton bind:filter />
```

### Todo.Svelte

Creamos archivo Todo.svelte: **43-create-Todo.svelte (Todo.svelte)**

lo importamos: **44-import-Todo.svelte (Todos.svelte)**
```javascript
import Todo from './Todo.svelte'
```

Y lo usamos en el markup: **45-use-Todo.svelte-from-Todos.svelte (Todos.svelte)**
```svelte
  <!-- Todos -->
  <ul role="list" class="todo-list stack-large" aria-labelledby="list-heading">
    {#each filterTodos(filter, todos) as todo (todo.id)}
      <li class="todo">
        <Todo {todo} />
      </li>
    {:else}
      <li>Nothing to do here!</li>
    {/each}
  </ul>
```

### Props-down, events-up pattern - remove todo

explicamos el patron

importamos createEventDispatcher: **46-import-createEventDispatcher (Todo.svelte)**
```javascript
  import { createEventDispatcher } from 'svelte'
  const dispatch = createEventDispatcher()
```

lo usamos en el boton de borrar: **47-dispatch-event-from-remove-button (Todo.svelte)**
```svelte
    <button type="button" class="btn btn__danger" on:click={() => dispatch('remove', todo)}>
      Delete <span class="visually-hidden">{todo.name}</span>
    </button>
```

y atendemos el evento en Todos.svelte: **48-handle-on-remove-event-from-Todos.svelte (Todos.svelte)**
```svelte
        <Todo {todo} on:remove={e => removeTodo(e.detail)}/>
```

### Update todo

codigo completo de Todo.svelte: **49-Todo.svelte-complete-implementation (Todo.svelte)**

updateTodo en Todos.svelte: **50-updateTodo-in-Todos.svelte (Todos.svelte)**
```svelte
  const updateTodo = (todo) => {
    const i = todos.findIndex(t => t.id === todo.id)
    todos[i] = todo
  }
```

atendemos evento on:update: **51-handle-on-remove-and-on-update-from-Todos.Svelte (Todos.svelte)**
Todos.svelte
```svelte
        <Todo {todo}
          on:remove={e => removeTodo(e.detail)}
          on:update={e => updateTodo(e.detail)}
        />
```

### Stores - 06_stores

creamos archivo src/stores.js: **60-create-stores.js (src/stores.js)**

creamos archivo components/Alert.svelte: **61-Alert.Svelte complete (Alert.svelte)**

importamos y usamos Alert.svelte desde App.svelte: **62-use-Alert.Svelte-from-App.svelte (App.svelte)**

actualizamos Alert.svelte para usar $store syntax: **63-update-Alert.svete-use-$store-syntax (Alert.svelte)**

#SNIP 63
Alert.svelte
```svelte
<script>
  import { alert } from '../stores.js'
</script>

{#if $alert}
<div on:click={() => $alert = ''}>
  <p>{ $alert }</p>
</div>
{/if}
```

actualizamos la seccion script de Todos.svelte y emitimos notificaciones: **64-emit-Alerts-from-Todos.svelte (Todos.svelte script section)**

actualizamos Alert.svelte (script y markup) para autoocultar las notificaciones **65-update-Alert.svelte-autoclear-message (Alert.svelte)**

