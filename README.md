# Svelte todo workshop

## Resources:

gitlab repo: https://gitlab.com/opensas/svelte-todo

docker hub gitpitch image: https://hub.docker.com/r/opensas/gitpitch

working application: https://todo-opensas.vercel.app/

slides on line: https://opensas.gitlab.io/svelte-todo/en/#/

Latest version of the slides: en/PITCHME.MD

## Steps to run locally

```shell
git clone git@gitlab.com:opensas/svelte-todo.git

cd svelte-todo

sudo docker run -it -v `pwd`:/repo -p 9000:9000 opensas/gitpitch:initial
```

> You can also run the `gitpitch_desktop.sh` script, or the `gitpitch_export_image.sh` to export the docker image.

## Links to open in the presentation

## Snippets

Snippets should be in the .vscode folder where you open vscode.

You should create a keyboard shortcut to invocate the snippets from vscode:

ctrl-shift-p - Preferences: Open Keyboard Shortcuts (JSON)

```json
{
  "key": "ctrl+shift+q",
  "command": "editor.action.showSnippets"
},
```

## Running the workshop

Go to the ./mozilla folder

run `vscode .`

(You will be using the snippets from ./mozilla/.vscode.mdn-svelte.code-snippets)

## Updating the slides and putting them online on gitlab pages

Update the corresponding PITCHME.md file

Run it locally (`gitpitch_desktop.sh`)

Open browser at `http://localhost:9000/`

Save the index.html page to ./public/new-site/index.html (it will be published on https://opensas.gitlab.io/svelte-todo/new-site)

Copy the assets and webfonts folders from ./public/nerdearla to ./public/new-site

Also save any image or other asset that has been added or updated.

Open index.html file and replace `index_files/` with `assets/`

Merge new context from `index_files` into `assets`

> Note: look for background files and adjust path, like this

```
... data-background-image="assets/gitpitch-audience.jpg"
```

Delete `index_file` folder

Test it with the local Live Server

Do the usual `git add ...` and `git push` to publish it on gitlab pages

Go to `https://gitlab.com/opensas/svelte-todo/-/pipelines` to check the deploy

You can update ./public/index.html to change the default presentation to redirect to

Test it at https://opensas.gitlab.io/svelte-todo/

---

Note: estos son los íconos que estamos usando: https://fontawesome.com/v5/search